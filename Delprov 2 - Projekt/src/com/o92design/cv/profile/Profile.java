package com.o92design.cv.profile;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import com.o92design.cv.components.EditableLabel;
import com.o92design.cv.components.EditablePanel;


public class Profile extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2653794633151386322L;
	
	private final int WIDTH = 512, HEIGHT = 944;
	private boolean editable;
	
	private Color backgroundColor = new Color(139, 159, 255);

	
	EditableLabel fullName;
	private int fullNameFontSize = 60;
	private Color fullNameColor = new Color(79, 97, 178);
	
	EditableLabel subTitle;
	private int relativeSizeToFullName = -40;
	private Color subTitleColor = new Color(193, 204, 255);
	
	private JLabel coverImage;
	private JLabel profileImage;
	
	AboutField aboutMe;
	EditablePanel knowledge;
	EditablePanel education;
	EditablePanel work;
	
	/**
	 * En container för våra olika komponenter som bygger upp en profil.
	 * Kan även sättas som editable för att ändra text i profilen.
	 * @param editable
	 */
	public Profile(boolean editable){
		this.editable = editable;
		System.out.println("Profile editable: " + this.editable);
		initialize();
	}

	/**
	 * Skapar profilen.
	 * @return
	 */
	public boolean initialize(){
		setBounds(0, 0, WIDTH, HEIGHT);
		setLayout(null);
		setBackground(backgroundColor);
		
		// Creates the knowledge field
		work = new EditablePanel(this, "Arbeten", 0, HEIGHT - 140, new Color(79, 97, 179), "/text/workList.txt");
		//Add it to this profile container
		add(work);	
		
		// Creates the education field
		education = new EditablePanel(this, "Utbildning", 0, HEIGHT - 240, new Color(116, 134, 215), "/text/education.txt");
		//Add it to this profile container
		add(education);
		
		// Creates the knowledge field
		knowledge = new EditablePanel(this, "Kunskaper", 0, HEIGHT - 340, new Color(164, 180, 255), "/text/knowledge.txt");
		//Add it to this profile container
		add(knowledge);	

		//Set fullname preferences
		this.fullName = new EditableLabel(this, "Namn", "Ditt namn här...", editable, backgroundColor);
		fullName.setForeground(fullNameColor);
		fullName.setHorizontalAlignment(SwingConstants.CENTER);
		fullName.setOpaque(false);
		fullName.setFont(new Font("Garamond", Font.BOLD, fullNameFontSize));
		fullName.setBounds(0, 0, 512, 110);
		//Add it to this profile container
		add(fullName);
		
		//Set the subTitle;
		subTitle = new EditableLabel(this, "Slogan", "Din slogan här...", editable, backgroundColor);
		subTitle.setForeground(subTitleColor);
		subTitle.setFont(new Font("Tahoma", Font.PLAIN, 25));
		subTitle.setHorizontalAlignment(SwingConstants.CENTER);
		subTitle.setBounds(0, 94, 512, 48);
		//Add it to this profile container
		add(subTitle);

		
		// Sets the preferences of profile Image
		profileImage = new JLabel("");
		profileImage.setIcon(new ImageIcon(Profile.class.getResource("/images/ProfilePicture.png")));
		profileImage.setBounds(264, 242, 200, 180);
		profileImage.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		//Add it to this profile container
		add(profileImage);
		
		// Sets the preferences of Cover image
		coverImage = new JLabel("");
		coverImage.setIcon(new ImageIcon(Profile.class.getResource("/images/coverImage.png")));
		coverImage.setBorder(new MatteBorder(5, 0, 5, 0, (Color) Color.DARK_GRAY));
		coverImage.setBounds(0, 153, 512, 256);
		//Add it to this profile container
		add(coverImage);
		
		// Creates the about me field
		aboutMe = new AboutField(this, 0, 422, 512, 164, "Om mig...", "Skriv en kort beskrivning av dig själv...", 17, backgroundColor);
		aboutMe.textArea.setMaxTextLength(215);
		//Add it to this profile container
		add(aboutMe);
		return true;
	}
	
	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		System.out.println(fullName.getText() + " är i editMode: " + editable);
		this.editable = editable;
	}

	public int getRelativeSizeToFullName() {
		return relativeSizeToFullName;
	}

	public void setRelativeSizeToFullName(int relativeSizeToFullName) {
		this.relativeSizeToFullName = relativeSizeToFullName;
	}
	
	
}
