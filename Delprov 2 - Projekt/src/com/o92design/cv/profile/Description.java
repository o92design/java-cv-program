package com.o92design.cv.profile;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Rectangle;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Description extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8942304634170654733L;
	
	private Rectangle area;
	private JLabel title;
	
	public Description(String title, Color color, int x, int y, int width, int height){
		setLayout(new FlowLayout());
		this.title = new JLabel(title);

		this.title.setBounds(0, 500, 120, 200);
		add(this.title);
		
		area = new Rectangle(x, y, width, height);
		setBounds(area);
		setBackground(color);
	}

}
