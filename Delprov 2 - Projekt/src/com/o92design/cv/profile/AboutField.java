package com.o92design.cv.profile;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.o92design.cv.components.EditableLabel;
import com.o92design.cv.components.EditableTextArea;

public class AboutField extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2127456308659547344L;

	private Profile parent;
	
	EditableLabel title;
	private String defaultTitleText;
	
	EditableTextArea textArea;
	private String defaultInfoText;
	
	private Color textColor = new Color(35, 40, 64);
	
	public AboutField(Profile parent, int x, int y, int width, int height, String defaultTitleText, String defaultInfoText, int infoTextSize, Color backgroundColor){
		this.defaultTitleText = defaultTitleText;
		this.defaultInfoText = defaultInfoText;
		this.parent = parent;

		setBounds(x, y, width, height);
		setLayout(null);
		setBackground(backgroundColor);
		
		// Skapar titel för om mig fältet.
		title = new EditableLabel(parent, "AboutArea", defaultTitleText, false, backgroundColor);
		title.setForeground(textColor);
		title.setOpaque(false);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(new Font("Arial Black", Font.PLAIN, 50));
		title.setBounds(0, 0, 512, 80);
		add(this.title);
		
		// Skapar textinfon om mig.
		textArea = new EditableTextArea(parent, "AboutText", "Skriv en kort beskrivning av dig här...", false, infoTextSize, 215);
		textArea.setForeground(textColor);
		add(this.textArea);
	}
	
	/**
	 * Kallar på {@link EditableTextArea} textFromTextFile metod med given adress för fil som parameter.
	 * @param filePath
	 */
	public void setInfoText(String filePath){
		textArea.textFromTextFile(filePath);
	}
	
	public EditableLabel getTitle() {
		return title;
	}
	public void setTitle(EditableLabel title) {
		this.title = title;
	}
	public String getDefaultTitleText() {
		return defaultTitleText;
	}
	public void setDefaultTitleText(String defaultTitleText) {
		this.defaultTitleText = defaultTitleText;
	}
	public EditableTextArea getTextArea() {
		return textArea;
	}
	public void setTextArea(EditableTextArea textArea) {
		this.textArea = textArea;
	}
	public String getDefaultInfoText() {
		return defaultInfoText;
	}
	public void setDefaultInfoText(String defaultInfoText) {
		this.defaultInfoText = defaultInfoText;
	}
	public Color getTextColor() {
		return textColor;
	}
	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}
	public Profile getParent() {
		return parent;
	}
}
