package com.o92design.cv.profile;

public class TestProfile extends Profile {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5138521825743202197L;

	public TestProfile(){
		super(false);
		
		fullName.setText("Oskar Månsson");
		subTitle.setText("- er nästa kollega");
		aboutMe.title.setText("Om mig");
		aboutMe.setInfoText("/text/aboutMe.txt");		
	}
}
