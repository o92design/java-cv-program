package com.o92design.cv.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import com.o92design.cv.core.Person;

public class Button extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2193706624152942164L;
	Command buttonCommand;
	private Person person;
	
	public Button(Person person, Command buttonCommand, String path, String rollOverPath, int x, int y , int width, int height){
		this.person = person;
		this.buttonCommand = buttonCommand;
		
		setIcon(new ImageIcon(getClass().getResource(path)));
		setRolloverIcon(new ImageIcon(Menu.class.getResource(rollOverPath)));
		setHorizontalTextPosition(SwingConstants.CENTER);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setFocusPainted(false);
		setBounds(x, y, width, height);	
		
		addActionListener(action());
			
	}
	
	public ActionListener action(){
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCommand.execute(person);
			}
		};
	}
	
}
