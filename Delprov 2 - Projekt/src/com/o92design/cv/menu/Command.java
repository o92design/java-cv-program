package com.o92design.cv.menu;

import com.o92design.cv.core.Person;

public interface Command {
	public void execute(Person person);
}
