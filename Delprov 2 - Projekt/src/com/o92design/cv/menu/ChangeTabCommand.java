package com.o92design.cv.menu;

import com.o92design.cv.core.Person;

public class ChangeTabCommand implements Command{

	private int tabIncrement;
	
	public ChangeTabCommand(int tabIncrement){
		this.tabIncrement = tabIncrement;
	}

	public void execute(Person person) {
		int index = person.getSelectedIndex() + tabIncrement;
		if(index == person.getSelectedIndex() || index <= -1 || index >= person.getTabCount())
			return;
		
		person.setSelectedIndex(index);
	}
	
}
