package com.o92design.cv.menu;

import java.awt.Color;

import javax.swing.JPanel;

import com.o92design.cv.core.Person;

public class Menu extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7297023322823445860L;

	Person person;
	
	Button left;
	ChangeTabCommand changeTabLeft;
	
	Button removeProfile;
	
	Button info;
	InfoCommand infoCommand;
	
	Button createProfile;
	
	Button right;
	ChangeTabCommand changeTabRight;
	
	public Menu(Person person, Color backgroundColor, int x, int y, int width, int height){
		this.person = person;
		setBounds(x, y, width, height);
		setBackground(backgroundColor);
		setLayout(null);
		initialize();
	}
	
	private void initialize(){

		changeTabLeft = new ChangeTabCommand(-1);
		left = new Button(person, changeTabLeft, "/images/ArrowLeftIcon.png", "/images/ArrowLeftIconHoover.png", 0, 0, 90, 81);
		add(left);
		
		infoCommand = new InfoCommand("/text/infoText.txt");
		info = new Button(person, infoCommand, "/images/InfoButtonIcon.png", "/images/InfoButtonIconHover.png", 512 / 2 - 55, 0, 114, 81);
		add(info);
		
		changeTabRight = new ChangeTabCommand(1);
		right = new Button(person, changeTabRight, "/images/ArrowRightIcon.png", "/images/ArrowRightIconHoover.png", 432, 0, 90, 81);
		add(right);
	}
}
