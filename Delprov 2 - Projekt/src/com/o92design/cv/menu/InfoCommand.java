package com.o92design.cv.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.swing.JOptionPane;

import com.o92design.cv.core.Person;

public class InfoCommand implements Command{
	String infoText;
	
	public InfoCommand(String filePath) {
		textFromTextFile(filePath);
	}
	
	
	public void execute(Person person) {
		JOptionPane.showMessageDialog(person.getParentFrame(), infoText, "Info", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void textFromTextFile(String filePath){
		InputStream file = getClass().getResourceAsStream(filePath);
		
		// Läser raderna från file InputStream.
		BufferedReader br = new BufferedReader(new InputStreamReader(file, StandardCharsets.UTF_8));
		
		// Bygger en stor String från raderna som br förmedlar.
		StringBuilder sb = new StringBuilder();
		
		// Sparar en rad i en sträng för att bygga en större sträng.
		String line = "";
		
		// Lägger raden från textfilen i String. Lägger sedan till strängen i större sb String.
		try {
			while((line = br.readLine()) != null){
				sb.append(line);
				
				// Om raden är slut så lägger vi till en ny tom rad för att lägga nästa rad under.
				if(line.length() > 0){
					sb.append("\n");					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Sätter komponentens text till vår nya stora String.
		infoText = sb.toString();
	}

}
