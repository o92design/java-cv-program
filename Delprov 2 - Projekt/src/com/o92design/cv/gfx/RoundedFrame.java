package com.o92design.cv.gfx;

import java.awt.geom.Path2D;

public class RoundedFrame extends Path2D.Float{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1562715596538230794L;

	public RoundedFrame(int x, int y, float width, float height, float thickness, float radius, boolean lowerRoundness) {

    	
    	/*
    	 * 	How the frame is drawn
    	 * 		 	1 ->
    	 *    .O1--------------O2.
    	 *    |  .I1_______I2.  |
    	 *    | /	 5->	  \ |
    	 *    |	'I8	  		I3' |
    	 *'/\ | |/\		     6| |  2
    	 * 4  | |8		    \/| | \/
    	 * 	  | .I7			I4. | 
    	 *    | \.I6__<-7__I5./ | 
    	 *    |					|
    	 *    !O4-------------O3!
    	 * 		 	<- 3
    	 */
    	
    	
    	// Where the frames upper left corner is located
        moveTo(x, y);
        
        // Step 1 draw line to OUTER second dot.
        lineTo(width, y);
        
        // Step 2 draw line to OUTER third dot.	  	
        lineTo(width, height);
        
        // Step 3 draw line to OUTER fourth dot.
        lineTo(x, height);
        
        // Step 4 draw line to OUTER first dot.
        lineTo(x, y);

        float innerWidth = width - thickness;
        float innerHeight = height - thickness;

        // INNER I1
        moveTo(thickness + radius, thickness);
        
        // Step 5 draw line to INNER second dot.
        lineTo(innerWidth - radius, thickness);
        
        // Curve from INNER second dot to INNER third dot.
        curveTo(innerWidth, thickness, innerWidth, thickness, innerWidth, thickness + radius);

        
        // Lower right corner.
        if(lowerRoundness){
        	// Step 6 draw line to INNER fourth dot.
            lineTo(innerWidth, innerHeight - radius);
            
        	// Curve from INNER fourth dot to INNER fifth dot.
        	curveTo(innerWidth, innerHeight, innerWidth, innerHeight, innerWidth - radius, innerHeight);	
        	
        	// Step 7 draw line to INNER sixth dot.
            lineTo(thickness + radius, innerHeight);
        }
        else{
        	// Step 6 draw line to INNER fourth dot.
            lineTo(innerWidth, innerHeight);
        	
        	 // Step 7 draw line to INNER sixth dot.
            lineTo(thickness, innerHeight);
        }
         
        // Lower left corner
        if(lowerRoundness){
        	// Curve from INNER sixth dot to INNER seventh dot.
            curveTo(thickness, innerHeight, thickness, innerHeight, thickness, innerHeight - radius);        
        }

        // Step 8 draw line to INNER eight dot.
        lineTo(thickness, thickness + radius);
        
        // Curve to INNER first dot.
        curveTo(thickness, thickness, thickness, thickness, thickness + radius, thickness);

        closePath();

        setWindingRule(WIND_EVEN_ODD);

    }
}
