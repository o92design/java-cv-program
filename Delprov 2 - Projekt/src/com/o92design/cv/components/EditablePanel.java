package com.o92design.cv.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.o92design.cv.gfx.RoundedFrame;
import com.o92design.cv.profile.Profile;

public class EditablePanel extends JPanel implements EditableComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5263486631436623000L;

	private Profile parent;
	
	private boolean inPanel = false;
	
	private String defaultTitle;
	
	private int panelHover = 50;
	private int inPanelYdecrement = 400;
	private final int HEIGHT = 250;
	
	private String listPath;
	private Vector<String> stuff;
	private Vector<EditableLabel> labels;
	
	private Color color = Color.GREEN;
	
	private int x, y;
	
	public EditablePanel(Profile parent, String defaultTitle, int x, int y, Color color, String listPath) {
		this.defaultTitle = defaultTitle;
		this.parent = parent;
		
		this.x = x;
		this.y = y;
		
		this.listPath = listPath;
		
		this.color = color;
		
		labels = new Vector<>();

		initialize();
		
	}
	
	public void initialize() {
		setBounds(x, y, 512, 500);
		setOpaque(false);
		setBackground(Color.BLACK);
		setLayout(null);
		
		JLabel title = new JLabel(defaultTitle);
		title.setHorizontalAlignment(SwingConstants.LEFT);
		title.setFont(new Font("Garamond", Font.PLAIN, 33));
		title.setForeground(Color.BLACK);
		title.setBounds(25, 10, 500, 50);
		add(title);
		
		stuff = listFromTextFile(listPath);
		buildList();
		
		addMouseListener(mouse());

	}
	/**
	 *  Bygger en lista av EditableLabels från vår vector med Strings.
	 *  @uses
	 *  {@link EditableLabel}
	 */
	private void buildList(){
		if(stuff == null || stuff.isEmpty())
			return;
		
		int y = 40;
		for(String string : stuff) {
			EditableLabel label = new EditableLabel(parent, string, string, false, null);
			label.setBounds(42, y + 20, 512, 20);
			label.setFont(new Font("Calibri", Font.PLAIN, 16));
			label.setHorizontalAlignment(SwingConstants.LEFT);
			label.setForeground(Color.BLACK);
			add(label);
			y += 20;
		}
	}
	
	/**
	 * Skapar en Vector med Strings från given klass path. 
	 * Denna är ungefär likadan som {@link EditableTextArea}'s textFromTextFile.
	 * Detta hade kunnat göras om till en egen klass som jag hade kunnat ropa på 
	 * för att läsa in textfiler. Det hade gjort koden mycket tydligare och lättare
	 * att underhålla. 
	 * @param filePath
	 * @return {@link Vector<String>}
	 */
	public Vector<String> listFromTextFile(String filePath){
		if(filePath == null || filePath == ""){
			return null;
		}
		Vector<String> lines = new Vector<>();
		
		try {
			InputStream file = getClass().getResourceAsStream(filePath);
			BufferedReader br = new BufferedReader(new InputStreamReader(file, StandardCharsets.UTF_8));
			String line = "";
			while((line = br.readLine()) != null){
				lines.add(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return lines;
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		// Skapar en rundad frame på plats x,y med hjälp av Grapghics2D
		Graphics2D g2D = (Graphics2D) g;
		RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2D.setRenderingHints(qualityHints);
		g2D.setColor(color);
		g2D.fill(new RoundedFrame(x, y, 512, HEIGHT, 0, 80, false));
		
	}
	
	public void setEditable() {
		// TODO in future feature: Change status of panel to add and remove stuff from list.
	}
	
	public MouseAdapter mouse() {
		return new MouseAdapter() {
			public void mouseEntered(MouseEvent e){
				if(!inPanel)
					setLocation(x, y - panelHover);
			}
			// Denna metod är buggad då den inte tar i beaktning ifall musen lämnar komponentens område 
			// relaterat till pixlar. Just nu så lämnar musen komponenten när den är över texten då 
			// texten är en egen komponent som ligger över panelen. 
			// Se utvärderings dokumentet för vidare dokumentering av hur man hade kunnat lösa detta.
			public void mouseExited(MouseEvent e) {
				if(!inPanel){
					if(e.getY() < 0)
						setLocation(x, y);
				}
			}
			public void mouseClicked(MouseEvent e){
				if(e.getButton() == MouseEvent.BUTTON1){
					inPanel = !inPanel;
					if(inPanel){
						setBounds(x, y - inPanelYdecrement, getWidth(), HEIGHT + inPanelYdecrement);
					}
					else{
						setBounds(x, y, getWidth(), HEIGHT);	
					}
				}
			}
		};
	}

	public KeyAdapter keyboard() {
		return null;
	}

	public Vector<EditableLabel> getLabels() {
		return labels;
	}



}
