package com.o92design.cv.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import com.o92design.cv.profile.Profile;

public class EditableTextArea extends JTextArea implements EditableComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9079481521098895863L;

	private Profile parent;
	
	private int maxTextLength;
	private boolean editMode;
	private final String defaultText;
	private int textSize;
	private Color textColor = new Color(35, 40, 64);
	/**
	 * En {@link EditableComponent} komponent som gör så att vi kan ändra texten i en textArea.
	 * @param parent
	 * @param name
	 * @param defaultText
	 * @param editMode
	 * @param textSize
	 * @param maxTextLength
	 * @author Oskar Månsson
	 */
	public EditableTextArea(Profile parent, String name, String defaultText, boolean editMode, int textSize, int maxTextLength) {
		setName(name);
		this.parent = parent;
		this.defaultText = defaultText;
		this.editMode = editMode;
		this.textSize = textSize;
		this.maxTextLength = maxTextLength;
		initialize();
	}
	
	public void initialize() {
		
		addMouseListener(mouse());
		addKeyListener(keyboard());
		
		setEditable(editMode);
		setText(defaultText);
		setForeground(textColor);
		setFont(new Font("Calibri", Font.PLAIN, textSize));
		setOpaque(false);
		setLineWrap(true);
		setWrapStyleWord(true);
		setBounds(68, 80, 377, 187);
		if(editMode){
			setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		}
		
	}
	/**
	 * Läser in rad efter rad från en textfil. Sparar raden i en string som lagras i 
	 * en vector. Sätter tillslut texten till raderna från textfilen.
	 * Denna är ungefär likadan som {@link EditablePanel}'s listFromTextFile.
	 * Detta hade kunnat göras om till en egen klass som jag hade kunnat ropa på 
	 * för att läsa in textfiler. Det hade gjort koden mycket tydligare och lättare
	 * att underhålla. 
	 * @param filePath
	 * @exception IOException
	 * @author Oskar Månsson
	 * @see {@link InputStream}, {@link BufferedReader}, {@link StringBuilder}, {@link String}
	 */
	public void textFromTextFile(String filePath){
		InputStream file = getClass().getResourceAsStream(filePath);
		
		// Läser raderna från file InputStream.
		BufferedReader br = new BufferedReader(new InputStreamReader(file, StandardCharsets.UTF_8));
		
		
		// Bygger en stor String från raderna som br förmedlar.
		StringBuilder sb = new StringBuilder();
		
		// Sparar en rad i en sträng för att bygga en större sträng.
		String line = "";
		
		// Lägger raden från textfilen i String. Lägger sedan till strängen i större sb String.
		try {
			while((line = br.readLine()) != null){
				sb.append(line);
				
				// Om raden är slut så lägger vi till en ny tom rad för att få ett mellanrum.
				if(line.length() > 0){
					sb.append("\n");					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			setText(defaultText);
			return;
		}
		
		// Kontrollerar ifall texten är för lång
		if(isTextTooLong(sb.toString())){
			setText(defaultText);
			if(!editMode){				
				setEditable();
			}
			return;
		}


		// Sätter komponentens text till vår nya stora String.
		setText(sb.toString());
	}

	public boolean isTextTooLong(String text){
		boolean tooLong = text.length() > maxTextLength;
		if(tooLong)
			System.err.println("Texten är för lång!\n"
					+ text.length() + "/" + maxTextLength);
		return tooLong;
	}
	
	public void setEditable(){
		// Kollar ifall texten är för lång i så fall sätt editMode till true, texten måste ändras.
		// Ifall inte så sätter den editMode till det omvända värdet av editMode.
		editMode = isTextTooLong(getText()) ? true : !editMode; 
			
		if(editMode){
			setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		}
		else{
			setBorder(null);
		}
		System.out.println(getName() + " in edit mode: " + editMode);	
		super.setEditable(editMode);
	}
	
	/**
	 * Creates a mouse that makes the text area editable when clicked upon
	 * with the right mouse button.
	 * @see {@link java.awt.event.MouseAdapter} {@link java.awt.event.MouseEvent}
	 */
	public MouseAdapter mouse(){
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent e){			
				if(parent.isEditable()){
					switch(e.getButton()){
					// Right mouse button was clicked. Configure the component to editable or uneditable.
					case MouseEvent.BUTTON3:
						setEditable();
						break;
					default:
						break;		
					}
				}
			}
		};
	}
	
	/**
	 * Creates a keyboard that goes out of edit mode when enter is pressed.
	 * @see {@link java.awt.event.KeyAdapter} {@link java.awt.event.KeyEvent}
	 */
	public KeyAdapter keyboard(){
		return new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
					switch(e.getKeyCode()){
					// Go out of the edit mode and null out the border of area.
					case KeyEvent.VK_ENTER:
							if(e.isControlDown() && editMode){
								setEditable();
							}
						break;
					}
			}
		};
	}
	public boolean isEditMode() {
		return editMode;
	}
	public int getMaxTextLength() {
		return maxTextLength;
	}
	public void setMaxTextLength(int maxTextLength) {
		this.maxTextLength = maxTextLength;
	}
}
