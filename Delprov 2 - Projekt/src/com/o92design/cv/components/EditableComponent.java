package com.o92design.cv.components;

import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;

/**
 * Ett interface för våra komponenter som bygger upp programmet.
 * 
 * @author Oskar Månsson
 * 
 */

public interface EditableComponent {
	/**
	 *  Används för att skapa komponenten till användarens preferenser.
	 */
	void initialize();
	
	/**
	 * Förändrar kompnentens status. Sätter en border runt ifall den är i editMode.
	 * Tar bort border ifall den inte är i editMode.
	 */
	abstract void setEditable();

	/**
	 * Sätter upp hur musen skall integrera med komponenten.
	 *  
	 * @return {@link MouseAdapter}
	 * @author Oskar Månsson 2016
	 */
	abstract MouseAdapter mouse();
	/**
	 * Sätter upp hur tangentbordet skall integrera med komponenten.
	 *  
	 * @return {@link KeyAdapter}
	 * @author Oskar Månsson 2016
	 */
	abstract KeyAdapter keyboard();
	
}
