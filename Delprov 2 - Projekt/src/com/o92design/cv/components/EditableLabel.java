package com.o92design.cv.components;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import com.o92design.cv.profile.Profile;


public class EditableLabel extends JTextField implements EditableComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8595921661110356141L;
	private Profile parent;
	private boolean editMode;
	private final String defaultText;
	
	public EditableLabel(Profile parent, String name, String defaultText, boolean editMode, Color backgroundColor){
		this.editMode = editMode;
		this.defaultText = defaultText;
		this.parent = parent;
		setName(name);
		initialize();
	}
	
	public void initialize() {
		addMouseListener(mouse());
		addKeyListener(keyboard());
		
		setEditable(editMode);
		setText(defaultText);
		setBorder(null);
		setOpaque(false);

	}

	public void setEditable(){
		editMode = !editMode;
		if(editMode){
			setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		}
		else{
			setBorder(null);
		}
		System.out.println(getName() + " in edit mode: " + editMode);	
		super.setEditable(editMode);
	}
	
	/**
	 * Creates a mouse that makes the label editable when clicked upon
	 * with the right mouse button.
	 */
	public MouseAdapter mouse(){
		return new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				if(parent.isEditable()){
					// Right mouse button was clicked
					if(e.getButton() == MouseEvent.BUTTON3){
						setEditable();
					}	
				}
			}
		};
	}
	
	/**
	 * Creates a keyboard that goes out of edit mode when enter is pressed.
	 */
	public KeyAdapter keyboard(){
		return new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				if(editMode){
					if(e.getKeyCode() == KeyEvent.VK_ENTER && e.isControlDown()){
						setEditable();
					}
				}
			}
		};
	}



}
