package com.o92design.cv.core;

import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import com.o92design.cv.components.EditableTextArea;
import com.o92design.cv.profile.Profile;
/**
 * Tillhandahåller profilen och det personliga brevet med 
 * hjälp av olika tabbar. 
 * @author madri
 *
 */
public class Person extends JTabbedPane{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3997876912600647938L;
	private JFrame parentFrame;
	Profile profile;

	public Person(JFrame parentFrame, String name, Profile profile){
		this.parentFrame = parentFrame;
		this.profile = profile;
		
		setBounds(0,0, profile.getWidth(), profile.getHeight());
		setName(name);
		addTab(name, profile);
		EditableTextArea personalLetter = new EditableTextArea(profile, "Personligt brev", "Skriv ditt personliga brev här...", false, 16, 2340);
		personalLetter.textFromTextFile("/text/personligtBrev.txt");
		personalLetter.setMargin(new Insets(10, 20, 10, 10));
		addTab("Personligt brev", personalLetter);
	}

	public JFrame getParentFrame() {
		return parentFrame;
	}
}
