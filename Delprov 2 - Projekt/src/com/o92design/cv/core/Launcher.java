package com.o92design.cv.core;

import java.awt.EventQueue;

public class Launcher {
	public static void main(String[] args){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProfileProgram window = ProfileProgram.getInstance();
					window.initialize();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
