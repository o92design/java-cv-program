package com.o92design.cv.core;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

import com.o92design.cv.menu.Menu;
import com.o92design.cv.profile.TestProfile;

public class ProfileProgram {

	//Frame should be 16 pixels wider than desired resolution
	private final int WIDTH = 518, HEIGHT = 1054;
	private final String TITLE = "CV program av Oskar Månsson";
	
	public static boolean editMode = false;
	private final static ProfileProgram instance = new ProfileProgram();
	
	private JFrame frame;
	private Color backgroundColor = new Color(139, 159, 255);

	private Menu menu;
	private Color menuColor = new Color(20,20,20);
	
	private ProfileProgram(){}
	
	public boolean initialize(){
		try{
			frame = new JFrame(TITLE);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setPreferredSize(new Dimension(WIDTH, HEIGHT));
			frame.pack();
			frame.setLayout(null);
			frame.setResizable(false);
			frame.setVisible(true);
			frame.setLocationRelativeTo(null);
			frame.setBackground(backgroundColor);
			

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		Person oskar = new Person(frame, "Oskar Månsson", new TestProfile());
		
		frame.getContentPane().add(oskar);
		frame.getContentPane().add(new Menu(oskar, menuColor, 0, 944, 512, 81));
		return true;
	}
	
	public static ProfileProgram getInstance() {
		return instance;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
	
	
}
